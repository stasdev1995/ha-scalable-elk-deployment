#!/bin/bash
yum update -y
yum install -y python3-pip
pip3 install --upgrade pip
pip3 install "ansible==2.9.17"
pip3 install ansible[azure]
/usr/local/bin/ansible-galaxy collection install azure.azcollection
#pip3 install -r ~/.ansible/collections/ansible_collections/azure/azcollection/requirements-azure.txt
mkdir ~/.azure
touch ~/.azure/credentials

echo "[default]" >> ~/.azure/credentials
echo "subscription_id=" >> ~/.azure/credentials
echo "client_id=" >> ~/.azure/credentials
echo "secret=" >> ~/.azure/credentials
echo "tenant=" >> ~/.azure/credentials
# https://learn.microsoft.com/en-us/azure/developer/ansible/install-on-linux-vm?tabs=azure-cli

/usr/local/bin/ansible-galaxy install elastic.elasticsearch,7.5.1
touch /home/adminuser/azure_rm.yaml

touch /home/adminuser/inventory
echo "[nodes]" >> /home/adminuser/inventory
echo "localhost" >> /home/adminuser/inventory

touch /home/adminuser/install_es.yml
echo "- name: Single es node cluster" >> /home/adminuser/install_es.yml
echo "  hosts: nodes"  >> /home/adminuser/install_es.yml
echo "  connection: local"  >> /home/adminuser/install_es.yml
echo "  roles:"  >> /home/adminuser/install_es.yml
echo "    - role: elastic.elasticsearch"  >> /home/adminuser/install_es.yml

/usr/local/bin/ansible-playbook -i /home/adminuser/inventory /home/adminuser/install_es.yml

# /usr/local/bin/ansible-inventory -i /home/adminuser/azure_rm.yaml --list

