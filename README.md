# My first system design
## Terraform - Gitlab-ci - Ansible - Azure - Blue Green Deployment

TABLE OF CONTENTS
-----------------

 * Introduction
 * Design Architecture Doc
 * Prerequisites
 * Maintainers

INTRODUCTION
------------

This system design, was my initial approach, which i didnt complete since it was a long process and requires many cloud resources which are costly, since i choose to go with Azure.

On a high level, my idea was to:

- Use `terraform` to provision resources on the cloud.
- Use `ansible` to deploy `elasticsearch` on these resources
- Use `gitlab-ci` to automate the process


SYSTEM DESIGN DOCUMENT
----------------------

Based on the system requirement of the `Cloud DevOps Engineer Assignment`, i will describe all my system components and how they would interact with each other.

## Requirements - From the Assignment

### Overview
![alt text](https://akentominas.com/wp-content/uploads/2023/05/Screenshot_54.png)

*>>>* [VISIT THE PIPELINE](https://gitlab.com/stasdev1995/ha-scalable-elk-deployment/-/pipelines/859326283n) *<<<*
Make sure to enable to tougle button "Show dependencies" to have a better understanding of the pipeline.

### The setup of the system is automated & version upgrades of the system are automated and interruption free


My system would use `gitlac-ci` in order to automate the entire deployment of the system. I already setup `Gitlab` to be the `terraform backend`. Basically the `terraform state` is kept on Cloudlab, and only when a change is pushed to gitlab, the `terraform init`, `terraform plan` & `terraform apply` are triggered as separate jobs on the terraform pipeline.

On the next `ci job`, the `gitlab-ci` pipeline, will trigger the `ansible remote server` to start the deployment of `elasticsearch` to the `green environment`.

The next `ci job` will test the health of the `green deployment`. 

The next `ci job` will change the Loab Balancer `backend pool` the the `green environment` if tests pass

The next `ci job` would be manually triggered, only after traffic starts flowing to the green environment, and when no errors are reported, the new feature will be deployed to the `blue environment` and then the Load Balancer will be switched to the `blue baackend pool`.

If the erros after switching traffic to `green` will occur, the pipeline will rollback, switch the Loab Balancer to the old version (`blue backned pool`).

### The solution to be high available and reliable

Using `terraform` we deploy a number of servers for each of the backend pools, (green and blue). Using the approach, the traffic is distributed accross multiple nodes and we have a second `disaster recovery` environment.

### Scalability: could you run 1’000 clusters with your design?

Yes, using this approach would be really easy to scale up to the number of servers we choose.

*Why?*

Because we can use dinamic number of server provisioning using `terraform`, and ansible has the [azurecollection ansible plugin](https://galaxy.ansible.com/azure/azcollection), which is dynamically picking up the `inventory` list from the environment we execute `playbooks` against, using for example OS tags of the servers. We could have servers with `environment: blue` tag and `environment: green` tags. The whole process is automated without any manual addition of extra servers, or manual update of the `ansible inventory` list.


PREREQUISITES
--------------

- Need a payed Azure subscription
- Need to configure Gitlab to be the terraform backend
- Need to install ansible on its single server on azure and install the azure plugin for ansible to update the inventory list dynamically


MAINTAINERS
-----------

Anastasios Kentominas <akentominas@gmail.com>