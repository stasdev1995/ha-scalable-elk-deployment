# Creating resource group to logically group resources
resource "azurerm_resource_group" "swiss" {
  name     = var.AZ_RG_NAME
  location = var.AZ_LOCATION
  tags = {
    environment = var.AZ_RESOURCE_TAG_ENV
    source      = var.AZ_RESOURCE_TAG_SOURCE
  }
}

resource "azurerm_virtual_network" "swiss_virtual_network" {
  name                = "swiss-network"
  location            = azurerm_resource_group.swiss.location
  resource_group_name = azurerm_resource_group.swiss.name
  address_space       = ["10.123.0.0/16"]

  tags = {
    environment = var.AZ_RESOURCE_TAG_ENV
    source      = var.AZ_RESOURCE_TAG_SOURCE
  }
}

resource "azurerm_network_security_group" "swiss_sg" {
  name                = "swiss_sg"
  location            = azurerm_resource_group.swiss.location
  resource_group_name = azurerm_resource_group.swiss.name

  tags = {
    environment = var.AZ_RESOURCE_TAG_ENV
    source      = var.AZ_RESOURCE_TAG_SOURCE
  }
}

resource "azurerm_network_security_rule" "swiss_srule" {
  name                        = "swiss_srule"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_ranges     = ["22", "80", "9200"]
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.swiss.name
  network_security_group_name = azurerm_network_security_group.swiss_sg.name
}

resource "azurerm_subnet" "swiss_subnet" {
  name                 = "demo-subnet"
  resource_group_name  = azurerm_resource_group.swiss.name
  virtual_network_name = azurerm_virtual_network.swiss_virtual_network.name
  address_prefixes     = ["10.123.1.0/24"]
}

resource "azurerm_subnet_network_security_group_association" "swiss_sg_assoc" {
  subnet_id                 = azurerm_subnet.swiss_subnet.id
  network_security_group_id = azurerm_network_security_group.swiss_sg.id
}

resource "azurerm_public_ip" "swiss_public_ip" {
  name                = "swiss_ansible_public_ip"
  resource_group_name = azurerm_resource_group.swiss.name
  location            = azurerm_resource_group.swiss.location
  allocation_method   = "Static"

  tags = {
    environment = var.AZ_RESOURCE_TAG_ENV
    source      = var.AZ_RESOURCE_TAG_SOURCE
  }
}

resource "azurerm_network_interface" "swiss_nic" {
  name                = "swiss_nic"
  location            = azurerm_resource_group.swiss.location
  resource_group_name = azurerm_resource_group.swiss.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.swiss_subnet.id
    private_ip_address_allocation = "Static"
    public_ip_address_id          = azurerm_public_ip.swiss_public_ip.id
  }

  tags = {
    environment = var.AZ_RESOURCE_TAG_ENV
    source      = var.AZ_RESOURCE_TAG_SOURCE
  }
}

################################# ANSIBLE VM CONFIGURATION #######################################

# will use the same vm to install Ansible but also install ES
resource "azurerm_linux_virtual_machine" "ansible_vm" {
  name                = "ansible"
  resource_group_name = azurerm_resource_group.swiss.name
  location            = azurerm_resource_group.swiss.location
  size                = "Standard_F2"
  admin_username      = "adminuser"
  admin_password      = "e$Gdftrt5654l!23"
  network_interface_ids = [
    azurerm_network_interface.swiss_nic.id,
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("../ssh_keys/akentomikey.pub")
  }

  tags = {
    environment = "ansilble-elk"
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7_9"
    version   = "latest"
  }

  custom_data = filebase64("../files/install_ansible.tpl")

}




############################ BLUE ENVIRONEMENT SETUP #################


# resource "azurerm_subnet" "blue_subnet" {
#   name                 = "blue_subnet"
#   resource_group_name  = azurerm_resource_group.swiss.name
#   virtual_network_name = azurerm_virtual_network.swiss_virtual_network.name
#   address_prefixes     = ["10.123.2.0/24"]
#   depends_on = [
#     azurerm_virtual_network.swiss_virtual_network
#   ]
# }

# resource "azurerm_subnet_network_security_group_association" "swiss_blue_sg_assoc" {
#   subnet_id                 = azurerm_subnet.blue_subnet.id
#   network_security_group_id = azurerm_network_security_group.swiss_sg.id
# }

# // This interface is for blue environment
# resource "azurerm_network_interface" "blue_interface" {
#   name                = "blue_interface"
#   location            = azurerm_resource_group.swiss.location
#   resource_group_name = azurerm_resource_group.swiss.name

#   ip_configuration {
#     name                          = "internal"
#     subnet_id                     = azurerm_subnet.blue_subnet.id
#     private_ip_address_allocation = "Dynamic"
#   }

#   depends_on = [
#     azurerm_virtual_network.swiss_virtual_network,
#     azurerm_subnet.blue_subnet
#   ]
# }

# resource "azurerm_linux_virtual_machine" "blue_vm" {
#   name                = "bluevm1"
#   resource_group_name = azurerm_resource_group.swiss.name
#   location            = azurerm_resource_group.swiss.location
#   size                = "Standard_F2"
#   admin_username      = "adminuser"
#   network_interface_ids = [
#     azurerm_network_interface.blue_interface.id,
#   ]

#   admin_ssh_key {
#     username   = "adminuser"
#     public_key = file("../ssh_keys/akentomikey.pub")
#   }

#   tags = {
#     environment = "blue"
#   }

#   os_disk {
#     caching              = "ReadWrite"
#     storage_account_type = "Standard_LRS"
#   }

#   source_image_reference {
#     publisher = "OpenLogic"
#     offer     = "CentOS"
#     sku       = "7_9"
#     version   = "latest"
#   }
# }



############################ LOAD BALANCER CONFIG ####################


# resource "azurerm_public_ip" "load_ip" {
#   name                = "load-ip"
#   location            = azurerm_resource_group.swiss.location
#   resource_group_name = azurerm_resource_group.swiss.name
#   allocation_method   = "Static"
#   #sku                 = "Standard"
# }

# resource "azurerm_lb" "elasticsearch_balancer" {
#   name                = "elasticsearch_balancer"
#   location            = azurerm_resource_group.swiss.location
#   resource_group_name = azurerm_resource_group.swiss.name
#   #sku                 = "Standard"
#   #sku_tier            = "Regional"
#   frontend_ip_configuration {
#     name                 = "frontend-ip"
#     public_ip_address_id = azurerm_public_ip.load_ip.id
#   }

#   depends_on = [
#     azurerm_public_ip.load_ip
#   ]
# }

# // Here we are defining the backend pool
# resource "azurerm_lb_backend_address_pool" "blue_pool" {
#   loadbalancer_id = azurerm_lb.elasticsearch_balancer.id
#   #resource_group_name = azurerm_resource_group.swiss.name
#   name = "blue_pool"
#   depends_on = [
#     azurerm_lb.elasticsearch_balancer
#   ]
# }

# resource "azurerm_lb_backend_address_pool_address" "blue_address" {
#   name                    = "blue_address"
#   backend_address_pool_id = azurerm_lb_backend_address_pool.blue_pool.id
#   virtual_network_id      = azurerm_virtual_network.swiss_virtual_network.id
#   ip_address              = azurerm_network_interface.blue_interface.private_ip_address
#   depends_on = [
#     azurerm_lb_backend_address_pool.blue_pool
#   ]
# }


# // Here we are defining the Health Probe
# resource "azurerm_lb_probe" "blue_probe" {
#   loadbalancer_id = azurerm_lb.elasticsearch_balancer.id
#   name            = "blue_probe"
#   port            = 80
#   protocol        = "Tcp"
#   depends_on = [
#     azurerm_lb.elasticsearch_balancer
#   ]
# }

# // Here we are defining the Load Balancing Rule
# resource "azurerm_lb_rule" "blue_rule" {
#   #resource_group_name            = azurerm_resource_group.swiss.name
#   loadbalancer_id                = azurerm_lb.elasticsearch_balancer.id
#   name                           = "blue_rule"
#   protocol                       = "Tcp"
#   frontend_port                  = 80
#   backend_port                   = 80
#   frontend_ip_configuration_name = "frontend-ip"
#   backend_address_pool_ids       = [azurerm_lb_backend_address_pool.blue_pool.id]
#   depends_on = [
#     azurerm_lb.elasticsearch_balancer
#   ]
# }
