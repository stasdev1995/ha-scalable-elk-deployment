# Azure Resource Group Name
variable "AZ_RG_NAME" {
  default = "swiss"
}

variable "AZ_LOCATION" {
  default = "West Europe"
}

# Resource Tags
variable "AZ_RESOURCE_TAG_ENV" {
  default = "Development"
}

variable "AZ_RESOURCE_TAG_SOURCE" {
  default = "Terraform"
}

# AZURE SPECIFIC VARIABLES
variable "SUBSCRIPTION_ID" {
  default = ""
}

variable "CLIENT_ID" {
  default = ""
}

variable "CLIENT_SECRET" {
  default = ""
}

variable "TENANT_ID" {
  default = ""
}

# AZURE SPECIFIC VARIABLES
variable "ARM_SUBSCRIPTION_ID" {
  default = ""
}

variable "ARM_CLIENT_ID" {
  default = ""
}

variable "ARM_CLIENT_SECRET" {
  default = ""
}

variable "ARM_TENANT_ID" {
  default = ""
}

